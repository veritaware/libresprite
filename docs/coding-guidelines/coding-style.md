# C++ Coding Style

Due to the fact we have a strong .Net background, we favour the coding style that's mostly compliant with default VS settings for C# described in [dotNet coding style](https://github.com/dotnet/runtime/blob/master/docs/coding-guidelines/coding-style.md) mixed with some [ReSharper default code inspection settings](https://confluence.jetbrains.com/display/ReSharper/Code+Inspection+Wiki).

1. Use four spaces of intentation (no tabs).
1. Use [Allman style](https://en.wikipedia.org/wiki/Indentation_style#Allman_style) braces, i.e. Place the opening brace in a new line, indented to the same level as its control statement (I'm not going to dwell into its pros and cons – just follow it).
1. Single-satement ifs can be written without bracing, but only when all the blocks associated with the `if`/`else if`/…/`else` statement are placed in a single line.
1. Though in C# projects we would normally start private fields' names with an underscore, in order to avoid confusion with compiler directives, we use `m_camelCase` for private fields.
1. Public fields / properties should use `PascalCase`.
1. Local variables should be named using `camelCase`.
1. Constants should use `PascalCase`.
1. Methods should all use `PascalCase` despite their visibility. Local functions should use `camelCase`.
1. Class members should be placed in `public` — `private` — `protected` order.
1. Avoid using `this` unless really necessary (if you successfully follow the naming conventions above, the need for using `this` drops significantly).
1. Use a `struct` only for passive objects that carry data; everything else is a `class`.
1. Prefer to use a `struct` instead of a pair or a tuple whenever the elements can have meaningful names.
1. Always specify field's visibility, despite `class` and `struct` having their default member access.
1. Prefer [composition over inheritance](https://en.wikipedia.org/wiki/Composition_over_inheritance)
1. Include headers in the following order: _Related header_, _C system headers_, _C++ standard library headers_, _other libraries' headers_, _project's headers_. Every section followed by a blank line.
1. Place your code in corresponding namespaces. Namespaces should have unique names based on the project name, and possibly its path, but can be shortened to avoid over-verbosity. Base namespace for this project is `vwr::libspr`.
1. Use `auto` keyword for iterations or when the variable type is rather obvious.
1. Don't rely on built-in integer type keywords like `long long` or `unsigned int`. Instead use types like `int64_t` and `uint32_t` that are defined in the `<cstdint>` header .
1. Avoid using raw pointers. Prefer to pass values by reference or smart pointers.
1. Avoid unnecessary memory copying when passing variables to functions. Integers and floating points are generally fine to be passed by value, but unless there is an actual need to work on a copy of an object, consider passing any parameters bigger than 8 bytes by reference (but remeber to always pass smart pointers by value as this ensures proper reference counting!).
1. In general, every `.cpp` file should have a corresponding `.h` file. There are some common exceptions, such as unittests and small `.cpp` files containing just a `main()` function.
1. Rather than relying on compilers' `#pragma once` extension, all header files should have `#define` guards to prevent multiple inclusion. The format of the symbol name should be _`<PROJECT>`_`_`_`<PATH>`_`_`_`<FILE>`_`_H`.
1. We allow use of `friend` classes and functions, within reason. Friends should usually be defined in the same file so that the reader does not have to look in another file to find uses of the private members of a class. A common use of `friend` is to have a `FooBuilder` class be a friend of `Foo` so that it can construct the inner state of `Foo` correctly, without exposing this state to the world. In some cases it may be useful to make a unittest class a friend of the class it tests. Friends extend, but do not break, the encapsulation boundary of a class. In some cases this is better than making a member public when you want to give only one other class access to it. However, most classes should interact with other classes solely through their public members.
1. Avoid defining macros, especially in headers; prefer inline functions, enums, and `const` variables. Name macros with a project-specific prefix. Do not use macros to define pieces of a C++ API.
1. Use `nullptr` for pointers, and `'\0'` for chars.
1. Prefer `sizeof(varname)` to `sizeof(type)`. `sizeof(varname)` will update appropriately if someone changes the variable type either now or later.
1. Use `TODO` comments for code that is temporary, a short-term solution, or good-enough but not perfect.
1. To prevent scope contamination, place your switch's `case` blocks in curly braces.
1. No spaces around period or arrow por pointers/references. Pointer operators do not have trailing spaces.
1. Place the asterisks/ampersands adjacent to variable type as in example: `char* c` or `const std::string& s`.
1. Never mix-in pointer decorations if declaring multiple variables in a single line: `int a, *b; //BAD!`
