# LibreSprite project branching strategy

During the development of this project we are using the [git-flow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) branching model, which means that the `master` branch should contain the latest working release, our main development branch is `develop` and to this branch you should create your pull requests. 


*  Any features should be developed in their separate branches following the `feature/feature-name` branch naming. If a certain feature is linked to a [JIRA](https://veritaware.atlassian.net/secure/RapidBoard.jspa?rapidView=1) task, it would be nice to include the ticket number in the branch name, e.g. `feature/LIBSPR-4-coding-guidelines`.
*  Any bugfixes meant for the next big release should follow `bugfix/bugfix-name` pattern. As above, if those are linked to JIRA ticket, it's a good practice to include it on the begining of bugfix name.
*  Once we decide we have our feature set prepared, we will start a release branch, following the `release/version` pattern with a `v` prefix, e.g. `release/v1.2`. Once the work on release is done, we merge it into `master` and tag the spcific commit with release's name, and then back-merge the changes into `develop`.
*  Hotfixes that are meant to be released immediately into production should follow the `hotfix/hotfix-name` pattern. JIRA ticket rules apply.
*  To make our life easier we recommend using (Sourcetree)[https://www.sourcetreeapp.com/] to manage your git work or install [git-flow](https://github.com/nvie/gitflow) on your system. A quick cheatsheet for git-flow can be found [here](https://danielkummer.github.io/git-flow-cheatsheet/index.html)
*  If unsure about release versioning, please check [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html).